//--------------------------------------
// Klasa Gra
//
// kontroluje silnik gry (Engine), zawiera metode wirtualna do wyswietlania planszy (tablicy 2D)
// dziedziczy z klasy Engine
// Autor: Bartosz Bugajski
//
// Historia zmian:
// Data utworzenia: 22/02/2019
//

#ifndef GRA_H_
#define GRA_H_

#include "Engine.h"

class Gra: public Engine
{
public:
	//konstruktory i destruktory
	Gra(int i, int j);
	virtual ~Gra();

	void SetStart(const char* plikstart);								
	
	void Start();														//zapetlona metoda ktora analizuje i wyswietla plansze
protected:
	virtual void View() = 0;											//wirtualna metoda 
};

#endif