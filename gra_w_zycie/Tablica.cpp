#include "Tablica.h"


Tablica::Tablica(int i, int j)
{
	rozmiari = j;
	rozmiarj = i;
	T2D = new Komorka *[rozmiari+2];
	for (int i = 0; i < rozmiari+2; i++)
	{
		T2D[i] = new Komorka[rozmiarj+2];
	}
	for (int i = 0; i < rozmiari+2; i++)
	{
		for (int j = 0; j < rozmiarj+2; j++)
		{
			T2D[i][j].SetStan(false);
		}
	}
}


Tablica::~Tablica()
{
	for (int i = 0; i < rozmiari+2; i++)
	{
		delete[] T2D[i];
	}
	delete[] T2D;
}
