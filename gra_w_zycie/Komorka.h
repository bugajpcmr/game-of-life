//--------------------------------------
// Klasa Komorka
//
// zawiera pole stan i metody dostepowe do pol
//
// Autor: Bartosz Bugajski
//
// Historia zmian:
// Data utworzenia: 21/02/2019
//

#ifndef KOMORKA_H_
#define KOMORKA_H_

class Komorka
{
public:
	//konstruktory i destruktory
	Komorka();

	//konstruktor niewirtualny, poniewaz nie dziedziczymy lecz zawieramy i nie ma pol dynamicznych
	~Komorka();												

	//metody dostepowe
	bool GetStan();											//zwraca stan komorki - 0 lub 1

	void SetStan(bool x);									//ustawia stan na 0 lub 1
	
private:
	//pola prywatne
	bool stan;												//okresla czy komorka zyje czy nie, 1 lub 0
};

#endif