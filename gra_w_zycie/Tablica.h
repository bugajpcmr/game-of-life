//--------------------------------------
// Klasa Tablica
//
// korzysta z metod klasy Komorka, tworzy tablice dynamiczna 2D, ktora sluzy jako plansza dla komorek 
// o rozmiarze rozmiari na rozmiarj
// jest dzedziczona przez Engine
//
// Autor: Bartosz Bugajski
//
// Historia zmian:
// Data utworzenia: 21/02/2019
// Dodanie pol - rozmiari, rozmiarj: 11/03/2019

#ifndef TABLICA_H_
#define TABLICA_H_

#include "Komorka.h"

class Tablica
{
public:
	//konstruktory i destruktory
	Tablica(int i, int j);
	virtual ~Tablica();										//konstruktor wirtualny, poniewaz jest to klasa z ktorej dziedziczymy

protected:
	//pola potrzebne do wyznaczenia rozmiaru planszy
	int rozmiari;											
	int rozmiarj;

	Komorka **T2D;											//plansza, tablica dwuwymiarowa
};


#endif
