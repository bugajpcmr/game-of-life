//--------------------------------------
// Plik zrodlowy - main.cpp
//
// Przykladowy plik main.cpp, stworzony by zaprezentowac jak dziala program
// 
// Autor: Bartosz Bugajski
//
// Historia zmian:
// Data utworzenia: 22/02/2019
// Dodanie odczytu danych z pliku: 12/03/2019
//
// Plik .ini ma strukture:
//
// [ilosc komorek]
// [wspolrzedna x komorki] [wspolrzedna y komorki]
//

//#include <iostream>
#include "Gra_txt.h"

int main()
{
	Gra_txt Gra1(90,20);
	Gra *A = &Gra1;
	A->SetStart("mojagra.ini");
	A->Start();
	return 0;
}