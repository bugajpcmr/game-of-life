#include "Gra.h"
#include <fstream>


Gra::Gra(int i, int j):Engine(i,j)
{
}


Gra::~Gra()
{
}

void Gra::SetStart(const char* plikstart)
{
	//wspolrzedne
	int x = 0;
	int y = 0;
	//liczba komorek ktore maja byc wpisane
	int ile_komorek = 0;

	std::ifstream file;
	file.open(plikstart);

	file >> ile_komorek;

	while (ile_komorek != 0)
	{
		file >> x;
		file >> y;
		this->T2D[y][x].SetStan(1);
		ile_komorek--;
	}

	file.close();
}

void Gra::Start()
{
	while (true)
	{
		View();
		Analiza();
	}
}

