//--------------------------------------
// Klasa Gra
//
// kontroluje silnik gry (Engine), zawiera metode wirtualna do wyswietlania planszy (tablicy 2D)
// dziedziczy z klasy Engine
// Autor: Bartosz Bugajski
//
// Historia zmian:
// Data utworzenia: 22/02/2019
//

#ifndef GRA_TXT_H_
#define GRA_TXT_H_

#include "Gra.h"

class Gra_txt: public Gra
{
public:
	Gra_txt(int i, int j);
	~Gra_txt();

	void View();
};

#endif