#include "Engine.h"


Engine::Engine(int i, int j):Tablica(i,j)
{
	TempTab = new Komorka *[rozmiari+2];
	for (int i = 0; i < rozmiari+2; i++)
	{
		TempTab[i] = new Komorka[rozmiarj+2];
	}
	for (int i = 0; i < rozmiari+2; i++)
	{
		for (int j = 0; j < rozmiarj+2; j++)
		{
			TempTab[i][j].SetStan(false);
		}
	}
}


Engine::~Engine()
{
	for (int i = 0; i < rozmiari+2; i++)
	{
		delete[] TempTab[i];
	}
	delete[] TempTab;
}

void Engine::KopiujTab()
{
	for (int i = 0; i < rozmiari+2; i++)
	{
		for (int j = 0; j < rozmiarj+2; j++)
		{
			TempTab[i][j].SetStan(T2D[i][j].GetStan());
		}
	}
}

int Engine::iluZywych(int i, int j)
{
	int sasiedzi = 0;
	if (TempTab[i][j - 1].GetStan() == 1) sasiedzi++;
	if (TempTab[i - 1][j].GetStan() == 1) sasiedzi++;
	if (TempTab[i - 1][j - 1].GetStan() == 1) sasiedzi++;
	if (TempTab[i + 1][j - 1].GetStan() == 1) sasiedzi++;
	if (TempTab[i + 1][j].GetStan() == 1) sasiedzi++;
	if (TempTab[i - 1][j + 1].GetStan() == 1) sasiedzi++;
	if (TempTab[i][j + 1].GetStan() == 1) sasiedzi++;
	if (TempTab[i + 1][j + 1].GetStan() == 1) sasiedzi++;
	return sasiedzi;
}

void Engine::Analiza()
{
	int sasiedzi;
	KopiujTab();

	for (int i = 1; i < rozmiari; i++)
	{
		for (int j = 1; j < rozmiarj; j++)
		{
			sasiedzi = 0;
			if (TempTab[i][j].GetStan() == 0)
			{
				sasiedzi = iluZywych(i, j);
				
				if (sasiedzi == 3)
				{
					T2D[i][j].SetStan(1);
				}
			}

			if (TempTab[i][j].GetStan() == 1)
			{
				sasiedzi = iluZywych(i, j);
				if (sasiedzi == 3 || sasiedzi == 2)
				{
					T2D[i][j].SetStan(1);
				}
				if (sasiedzi > 3 || sasiedzi < 2)
				{
					T2D[i][j].SetStan(0);
				}
			}
		}
	}
}


