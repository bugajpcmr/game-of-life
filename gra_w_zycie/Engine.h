//--------------------------------------
// Klasa Engine
//
// jest silnikiem gry, analizuje tablice komorek, sprawdza ich stan i ilu ma sasiadow, 
// zawiera tablice zapamietujaca stany
// jest dzedziczona przez klase Gra
//
// Autor: Bartosz Bugajski
//
// Historia zmian:
// Data utworzenia: 21/02/2019
//

#ifndef ENGINE_H_
#define ENGINE_H_

#include "Tablica.h"

class Engine: public Tablica
{
public:
	//konstruktory i destruktory
	Engine(int i, int j);
	virtual ~Engine();

	//metody operujace na tablicach 2 wymiarowych
	int iluZywych(int i, int j);								//metoda sprawdzajaca ilu zywych sasiadow ma komorka
	void Analiza();												//sprawdza czy komorka ma sasiadow i na tej podstawie okresla stan komorki
	void KopiujTab();											//tworzy kopie tablicy
protected:
	Komorka **TempTab;											//tablica zapamietujaca stany poprzednie komorek
};

#endif